@extends('layouts.masterpage')
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
@section('content')
<style>
  body{
    background-color:pink;
  }
.btn:focus, .btn:active, button:focus, button:active {
  outline: none !important;
  box-shadow: none !important;
}

#image-gallery .modal-footer{
  display: block;
}

.thumb{
  margin-top: 15px;
  margin-bottom: 15px;
}.img-nail{

  width: 250px;
  height: 250px;
  
}

</style>
<script>

      $(document).ready(function(){
        $("#hide").click(function(){
          $("#imgnail").hide();
        });
        $("#show").click(function(){
          $("#imgnail").show();
        });
      });

      $('#datepicker-disabled-days').datetimepicker({
          daysOfWeekDisabled: [0, 6]
        });

</script>

<div class="container">
<form action="{{ route('insertbooking') }}" method="post" name="from_booking">
    {{ csrf_field() }}
  <div class="form-group margin">
    <label for="formGroupExampleInput">Name</label>
    <input type="text" name='cus_name' class="form-control" id="name" placeholder="Name">
    <div class="row" style="margin-top:1%">
        <div class="col">
          <label>Phone Number</label>
          <input type="text" name="cus_tel" class="form-control" placeholder="Phone Number" id="tel">
        </div>
        <div class="col">
          <label>Slip Id</label>
          <input type="text" name="txt_slip" class="form-control" placeholder="Slip Id" id="txt_slip">
        </div>
        <div class="col">
        <label>OneChat ID</label>
          <input type="text" name="cus_onechat_id" class="form-control" placeholder="OneChat ID" id="onechat">
    </div> 
    </div>
      <!-- Date and time picker with disbaled dates -->
      <br/>
      <label class="control-label" for="datepicker-disabled-days">Select Date and Time</label>
      {{-- <p type="text" class="form-control" id="date"></p> --}}
    <div class="row" style="margin-top:1%">
      <div class="col">
          <label>Date</label>
      <input type="date" name="date"  class="form-control" id="myDate" value="now()">
      </div>
      <div class="col">
          <label>Time</label>
          <input type="time" name="time" class="form-control" id="myTime" value="00:00:00">
      </div>
      <div class="col">
        <label>กรุณากดยืนยัน</label><br/>
          <button type="button" class="btn btn-success btn-lg"  onclick="myFunction()">จอง</button>
          
      </div>
  </div>
    <div class="col">
          <label  id="date" name="date"></label>
          <label  id="time" name="time"></label>
      </div>
            <script>
            function myFunction() {
              var x = document.getElementById("myTime").value;
              document.getElementById("time").innerHTML = x;
              
              var y = document.getElementById("myDate").value;
              document.getElementById("date").innerHTML = y;
            }
            </script>
    <button id="show"   type="button" class="btn btn-lg btn-primary" style="margin:10px; padding:10px">ต้องการเลือกลายจากทางร้าน</button>
    <button id="hide"  type="button" class="btn btn-lg  btn-warning" style="margin:10px; padding:10px">ไม่ต้องการเลือกลายจากทางร้าน</button>
    <div class="container">
	<div class="row" id="imgnail">
     <h3>กรุณาเลือกลายเล็บ</h3>
		
            {{-- <div class="col-6 col-md-4"> --}}
                <div class="row">
                @foreach($nails as $key => $nail)
                
                    <div class=".col-6 .col-md-4" style="margin:1%" href="#" data-image-id="" data-toggle="modal" data-title=""
                        data-image="{{$nail->nail_img}}"
                        data-target="#image-gallery">
                        <img  id="nailid"  type="button" class="img-nail"
                         src="{{$nail->nail_img}}"
                         alt="Another alt text" onclick="getIdImg({{ $nail->id }})">
                    </div>
                @endforeach
              </div>
            {{-- </div>--}}
        
        
        <input type="hidden" name="nail_id" id="getnailid" value="-">
        <script>
            function getIdImg(id) {
              // var z = document.getElementById("imgnail").value;
              alert('เลือกรูปแบบที่ ' + id);
              // document.getElementById("getnailid").innerHTML = z;
              document.from_booking.nail_id.value = id;
            }
        </script>
    </div>
    </div>
    <input type="submit" value="ยืนยันการจอง" class="btn btn-primary btn-lg" style="margin-top:2%; display-button:cente;">
    {{-- <button type="button" class="btn btn-primary btn-lg" type="submit">ยืนยันการจอง</button> --}}
  </form>
</div>
@endsection


