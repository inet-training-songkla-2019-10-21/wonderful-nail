<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>WonderfulNail</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">

                <!-- Bootstrap core CSS -->
        <link href="css/bootstrap.min.css" rel="stylesheet">

        <!-- Custom fonts for this template -->
        <link href="https://fonts.googleapis.com/css?family=Catamaran:100,200,300,400,500,600,700,800,900" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Lato:100,100i,300,300i,400,400i,700,700i,900,900i" rel="stylesheet">

        <!-- Custom styles for this template -->
        <link href="css/one-page-wonder.min.css" rel="stylesheet">
        <link href="css/one-page-wonder.css" rel="stylesheet">
    </head>
    <body>
        <!-- Navigation -->
        <nav class="navbar navbar-expand-lg navbar-dark navbar-custom fixed-top">
            <div class="container">
            <a class="navbar-brand" href="#">Wonderful Nail</a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarResponsive">
                <ul class="navbar-nav ml-auto">
                <li class="nav-item">
                    <a class="nav-link" href="{{ route('login') }}">Admin</a>
                </li>
                {{-- <li class="nav-item">
                    <a class="nav-link" href="#">Log In</a>
                </li> --}}
                </ul>
            </div>
            </div>
        </nav>

        <header class="masthead text-center text-white">
            <div class="masthead-content">
            <div class="container">
                <h1 class="masthead-heading mb-0">WELCOME</h1>
                <h2 class="masthead-subheading mb-0">Wonderful Nail</h2>
            <a href="{{ route('booking') }}" class="btn btn-primary btn-xl rounded-pill mt-5">จองเลย!!</a>
            </div>
            </div>
            <div class="bg-circle-1 bg-circle"></div>
            <div class="bg-circle-2 bg-circle"></div>
            <div class="bg-circle-3 bg-circle"></div>
            <div class="bg-circle-4 bg-circle"></div>
        </header>

        <section>
            <div class="container">
            <div class="row align-items-center">
                <div class="col-lg-6 order-lg-2">
                <div class="p-5">
                    <img class="img-fluid rounded-circle" src="img/nicnail-13.jpg" alt="">
                </div>
                </div>
                <div class="col-lg-6 order-lg-1">
                <div class="p-5">
                    <h2 class="display-4">Nail Art</h2>
                    <p>สำหรับการบริการที่ได้ค่อนข้างได้รับความนิยมอย่างมากคือ การตกแต่งเล็บ มีด้วยกันหลายประเภทให้เลือก ไม่ว่าจะเป็นการตกแต่งตามเทรนด์แฟชั่น, งานเพ้นท์ลวดลายงานศิลปะ, ติดเครื่องประดับเก๋ ๆ, ติดกลิตเตอร์อลังการ หรือโทนสีแบบสุภาพ ก็ทำออกมาได้สวยงามโดยช่างฝีมือดี และมีกาารออกแบบลายใหม่ ๆ ให้เลือกกันทุกอาทิตย์

                        </p>
                </div>
                </div>
            </div>
            </div>
        </section>

        <section>
            <div class="container">
            <div class="row align-items-center">
                <div class="col-lg-6">
                <div class="p-5">
                    <img class="img-fluid rounded-circle" src="img/nicnail-2.jpg" alt="">
                </div>
                </div>
                <div class="col-lg-6">
                <div class="p-5">
                    <h2 class="display-4">Enjoy One Stop Service For Beautiful Nails</h2>
                    <p>การทำเล็บเป็นกิจกรรมที่สาว ๆ หลายคนทำกันเป็นประจำเพื่อดูแลสุขภาพของเล็บและตกแต่งเพื่อความสวยงาม และยังบ่งบอกสไตล์ความเป็นตัวเองได้อีกด้วย บางคนอาจมีร้านประจำในดวงใจ แต่ถ้าใครกำลังมองหาร้านทำเล็บที่ดูแลแบบ Full Service ครบวงจร ตั้งแต่การดูแลสุขภาพเล็บจนไปถึงการตกแต่งทำลวดลายสวยงามที่อัพเดตเทรนด์การทำเล็บใหม่ ๆ อยู่เสมอ </p>
                </div>
                </div>
            </div>
            </div>
        </section>

        <section>
            <div class="container">
            <div class="row align-items-center">
                <div class="col-lg-6 order-lg-2">
                <div class="p-5">
                    <img class="img-fluid rounded-circle" src="img/nicnail-10.jpg" alt="">
                </div>
                </div>
                <div class="col-lg-6 order-lg-1">
                <div class="p-5">
                    <h2 class="display-4">Time To Get Your Nails Done</h2>
                    <p>น่าแปลกใจที่การเข้ามาใช้บริการของ Wonderfull Nail เต็มไปด้วยบรรดาผู้ชายพอ ๆ กันกับผู้หญิง เพราะการบริการค่อนข้างหลากหลายที่ค่อนข้างตอบโจทย์การใช้บริการแบบ Nail Salon ไม่ว่าเป็น Spa Nail (1,400 บาท) ที่ดูเเลทำความสะอาดเล็บมือและเท้าของเราทั้งแบบ Manicure และ Pedicure ตั้งแต่ การตะไบขอบเล็บให้เป็นทรงที่ต้องการ ตัดแต่งหนังส่วนเกิน นวดมือและทาน้ำยาบนเล็บ และการทาโลชั่นเพื่อถนอมมือ</p>
                </div>
                </div>
            </div>
            </div>
        </section>

        <!-- Footer -->
        <footer class="py-5 bg-black">
            <div class="container">
            <p class="m-0 text-center text-white small">Copyright &copy; Your Website 2019</p>
            </div>
            <!-- /.container -->
        </footer>

        <!-- Bootstrap core JavaScript -->
        <script src="vendor/jquery/jquery.min.js"></script>
        <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

    </body>
</html>
