<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Nail;
use Faker\Generator as Faker;

$factory->define(Nail::class, function (Faker $faker) {
    return [
        'nail_img' => $faker->randomElement([
            'https://i.pinimg.com/originals/f0/57/84/f0578459c9a7d7bb4d5cf59005df9b3b.jpg',
            'https://allnailart.com/wp-content/uploads/2019/01/ombre-nails-desings-1024x832.jpg',
            'http://www.tigerfeng.com/wp-content/uploads/2019/08/6-Sunflower-Nail-Designs.jpg',
            'https://f.herstylecode.com/2019/01/herstylecode-8.jpg',
            'http://www.emilydenisephotography.com/photos/IMG_4830PNS.jpg',
            'https://easyday.snydle.com/files/2016/02/easter-nail-art-designs-31.jpg',
            'http://www.prettydesigns.com/wp-content/uploads/2018/01/nail-art-designs-nail-design-ideas-16.jpg',
            'https://ideastand.com/wp-content/uploads/2016/01/red-and-black-nail-designs/1-red-black-nail-designs.jpg',
            'https://ideastand.com/wp-content/uploads/2016/01/red-and-black-nail-designs/5-red-black-nail-designs.jpg',
            'https://ideastand.com/wp-content/uploads/2016/01/glitter-nail-designs/3-glitter-nail-art-designs.jpg',
            'https://ideastand.com/wp-content/uploads/2016/01/pink-and-black-nail-art-designs/15-pink-and-black-nail-art-designs.jpg',
            'https://ideastand.com/wp-content/uploads/2016/01/pink-and-black-nail-art-designs/18-pink-and-black-nail-art-designs.jpg',
            'https://ideastand.com/wp-content/uploads/2015/12/christmas-nails/53-christmas-nail-art-designs.jpg',            
            'https://withfashion.net/storage/355/44707925_2140742395965231_6470419566991652617_n.jpg',
            'https://withfashion.net/storage/409/47693948_508574119673823_3490090086603593404_n.jpg',
            'https://withfashion.net/storage/410/46817772_268062417207590_8547286190639697913_n.jpg',          
                  

        ])
    ];
});
