<?php

namespace App;
use App\Booking;
use Illuminate\Database\Eloquent\Model;

class Nail extends Model
{
    protected $fillable = [
        'nail_img',
    ];
    public function bookings()
    {
        return $this->belongsTo(Booking::class, 'id' , 'id');
    }
}
