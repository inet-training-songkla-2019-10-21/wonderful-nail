<?php

namespace App\Http\Controllers;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\ConnectException;
use App\Booking;
use App\Customer;
use App\Nail;
use Illuminate\Support\Facades\Route;
use Illuminate\Http\Request;
use DB;



class BookingController extends Controller
{
    public function showBookingPage()
    {
        return view('booking.page');
        // return view('testbooking');
    }

    public function showNailPic()
    {
        $nails = Nail::all();

        return view('booking', compact('nails'));
        // ที่ form action ไปที่ -> insertbooking

        // return view('testbooking', compact('nails'));
    }
    public function booking(Request $request)
    {  
   
        // dd($request->cus_name);

        // $bookings = DB::table('bookings')->get();
        // $bookings = Booking::all();
        $bookings = Booking::where([
            ['time', '=' , $request->time ],
            ['date', '=' , $request->date ],
        ])->get();

        // dd($bookings);
        if (count($bookings)>0) {
            return redirect()->back();
        }else{
          
            $req_customer = new Customer();
            $req_customer->cus_name = $request->cus_name;
            $req_customer->cus_tel = $request->cus_tel;
            $req_customer->cus_onechat_id = $request->cus_onechat_id;
            $req_customer->created_at = now();

            if ($req_customer->save()) {
                    $customer = Customer::where('cus_name',$request->cus_name)->get()->first();
                    // dd($customer);
                $req_booking = new Booking();
                $req_booking->cus_id = $customer->id;
                $req_booking->date = $request->date;
                $req_booking->time = $request->time;
                $req_booking->nail_id = $request->nail_id;
                $req_booking->txt_slip = $request->txt_slip;
                $req_booking->status = 0;
                $req_booking->created_at = now();

                if ($req_booking->save()) {
                     return redirect()->route('index');
                }
            }
        }
    }
}
