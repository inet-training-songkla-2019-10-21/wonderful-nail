<?php

namespace App;
use App\Booking;
use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{
    protected  $fillable = [
        'cus_name',
        'cus_tel',
        'cus_onechat_id',
    ];
    
    public function bookings()
    {
        return $this->hasMany(Booking::class, 'cus_id','id');
    }
}
