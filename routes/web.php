<?php


Route::get('/', function () {
    return view('index');
})->name('index');

// Route::get('/dashboard', function () {
//     // return view('welcome');
//     return view('dashboard');
// });


Route::get('/booking', 'BookingController@showBookingPage')->name('booking.page');

Route::get('/booking', 'BookingController@showNailPic')->name('booking');
// Route::get('/booking/bk', 'BookingController@booking')->name('insertbooking');

Auth::routes();

// Route::get('/home', 'HomeController@index')->name('home');

Route::get('/homeadmin', 'Admin\AdminController@dashboard')->name('dashboardPage');
Route::get('/homeadmin/approve/{id}', 'Admin\AdminController@approved')->name('approve');
Route::get('/homeadmin/delete/{id}', 'Admin\AdminController@delete')->name('delete'); // เผื่อใช้
Route::get('/onechat/message/{id}', 'Admin\AdminController@sendMsg')->name('sendMsg');



Route::get('/login','Admin\LoginController@showLoginFrom')->name('login');
Route::post('/login', 'Admin\LoginController@login');
Route::get('logout','Admin\LoginController@logout')->name('logout');

Route::post('booking', 'BookingController@booking')->name('insertbooking');