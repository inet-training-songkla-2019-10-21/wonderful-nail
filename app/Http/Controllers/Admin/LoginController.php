<?php

namespace App\Http\Controllers\Admin;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Http\Request;

class LoginController extends Controller
{
    public function showLoginFrom()
    {
        // dd(10);
        return view('auth.login');
    }
    public function login(Request $request)
    {
        // dd(8);
        $admin = User::where('username',$request->username)->get()->first();
      
   // dd($admin);
        if (!$admin) {
        //    dd(1);
            return redirect()->back();
        }
        // dd(2);
        return redirect()->route('dashboardPage');
    }
    public function logout()
    {
        Auth::logout();
        return Redirect()->route('index');
    }
}