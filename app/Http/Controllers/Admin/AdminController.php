<?php

namespace App\Http\Controllers\Admin;

use App\Booking;
use App\Customer;
use App\Http\Controllers\Controller;
use App\Nail;
use Illuminate\Http\Request;
use DB;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\ConnectException;

class AdminController extends Controller
{public function dashboard()

    {
        $book = DB::table('bookings')
                ->join('nails', 'bookings.nail_id', '=', 'nails.id')
                ->join('customers', 'bookings.cus_id' , '=' , 'customers.id')
                ->select('bookings.*', 'customers.cus_name','customers.cus_tel','nails.nail_img')
                ->get();


        return view('dashboard',compact('book'));
    }
   
    public function approved(Request $request)
    {
        $booking = Booking::find($request->id);


        DB::table('bookings')
                ->where('id', $request->id)
                ->update(['status' => 1]);

        return redirect()->route('sendMsg',$booking->id);
    }
    public function sendMsg($id)
        // ติดตั้ง composer require guzzlehttp/guzzle แล้วเรียกใช้ use GuzzleHttp\Client;use GuzzleHttp\Exception\ConnectException;
        // 'to' => Id Onechat ปลายทาง 
    {

        $data = DB::table('bookings')
                ->join('nails', 'bookings.nail_id', '=', 'nails.id')
                ->join('customers', 'bookings.cus_id' , '=' , 'customers.id')
                ->select('bookings.*', 'customers.cus_name','customers.cus_tel','nails.nail_img')
                ->where('bookings.id', '=', $id)
                ->get();

        // dd($data);
        $message = "คุณ " . $data[0]->cus_name . " ได้ทำรายการจองทำเล็บในวัน " . $data[0]->date . " เวลา " .  $data[0]->time . " SlipID: " . $data[0]->txt_slip . "    ขอบคุณที่ใช้บริการ";
        // dd($message);

        $data = [
            'headers' => [
                'Authorization' => 'Bearer A7fdf4a581acd58e19e498ea6c7a570f12d2e9caee48844148831398ded7356f7f3d032e4c43b463e91e7194e72711e9d',
                'Content-Type' => 'application/json'
            ],
            'json' => [
                'to' => 'U105d089997be516293876db9f589d046',
                'bot_id' => 'Bd02cbb894ca8507e98067f3f7abd1a48',
                'type' => 'text',
                'message' => $message
            ]
        ];
        
        try {
            $client = new Client([
                'base_uri' => 'https://chat-public.one.th:8034/api/v1/',
                'verify' => false,
            ]);

            $res = $client->request('POST','push_message',$data)->getBody()->getContents();

        } catch (ConnectException $e) {
            return $e->getMessage();
        }
        // return response()->json(json_decode($res));
        return redirect()->route('dashboardPage');
    }
     //  เผื่อใช้
     public function delete(Booking $id)
     {
         $id->delete();
         return redirect()->route('dashboardPage');
     }
}
