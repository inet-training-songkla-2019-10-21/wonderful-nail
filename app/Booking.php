<?php

namespace App;
use App\Customer;
use App\Nail;
use Illuminate\Database\Eloquent\Model;

class Booking extends Model
{
    protected  $fillable = [
        'cus_id',
        'date',
        'time',
        'nail_id',
        'img_slip',
        'status'
    ];

    public function customers()
    {
        return $this->belongsToMany(Customer::class, 'cus_id' , 'id');
    }
    public function nails()
    {
        return $this->hasOne(Nail::class, 'nail_id' , 'id');
    }
}
